import pandas as pd 
import numpy as np 
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy.cluster.hierarchy import dendrogram

def visualization(data, label, feature, target):
    #create Dataframe
    # print(data[0])
    df = pd.DataFrame(np.column_stack((data,label)),columns=(feature+[target]))
    # print(data[0])
    visualizationPer2Feature(df,feature,target)
    visualization3D(df,feature,target)
    pass

def visualizationPer2Feature(dataframe,feature,target):
    pp = sns.pairplot(dataframe,vars=dataframe.columns[:-1],hue=target,plot_kws=dict(edgecolor="black", linewidth=0.5),height=1.8, aspect=1.8)
    fig = pp.fig
    fig.subplots_adjust(top=0.93 , wspace=0.3)
    t = fig.suptitle('Attributes Pairwise Plots', fontsize=14)

def visualization3D(df,feature,target):
    fig = plt.figure(figsize=(10,6))
    ax = fig.add_subplot(111, projection='3d')
    cols = feature + [target]
    x,y,z,size = (df[elmt].values for elmt in feature)
    color = df[target].values
    sc = ax.scatter(x,y,z,c=color,s=size*100)
    legend1 = ax.legend(*sc.legend_elements(),loc="lower left", title="Classes")
    ax.add_artist(legend1)
    handles, labels = sc.legend_elements(prop="sizes", alpha=0.6)
    legend2 = ax.legend(handles, labels, loc="upper right", title=(feature[3]+"\n*100"))
    ax.add_artist(legend2)
    ax.set_xlabel(feature[0])
    ax.set_ylabel(feature[1])
    ax.set_zlabel(feature[2])
    ax.legend()

def HAC_dendogram(Z):
    fig = plt.figure()
    dn = dendrogram(Z)
    plt.show()
