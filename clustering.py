import numpy as np 
import pandas as pd 
import math
from copy import copy
# HAC class
class HAC(object):
    

    def __init__(self, n_cluster, linkage,distance_threshold=None):
        self.methods = ['single','complete','average','centroid']
        self.n_cluster = n_cluster
        self.distance_threshold = distance_threshold
        self.linkage = linkage
    
    def distance (self,x1,x2,column_size=0):
        result = 0
        for j in range(len(x1)):
            result += math.pow((x1[j]-x2[j]),2)
        return math.sqrt(result)

    def centroid (self,index_list):
        return np.mean(([self.data[i] for i in index_list]),axis=0)

    def proximityMatrix(self,data):
        row_size, column_size = data.shape
        matrix = np.array([[self.distance(data[i],data[j],column_size) for i in range(row_size)] for j in range(row_size)])
        return matrix
    
    def update2RowCol(self, index_1, index_2,value):
        for i in range (self.distance_matrix.shape[0]):
            self.distance_matrix[i][index_1] = value[i]
            self.distance_matrix[index_1][i] = value[i]
            self.distance_matrix[i][index_2] = value[i]
            self.distance_matrix[index_2][i] = value[i]

    def updateDistance(self,index_1,index_2):
        
        col_size = self.distance_matrix.shape[0]
        if self.linkage is 'single':
            # Calculate New Distance
            new_distance = [min(self.distance_matrix[index_1][j],self.distance_matrix[index_2][j]) for j in range(col_size)]
            new_distance[index_1] = 0
            new_distance[index_2] = 0
            self.update2RowCol(index_1,index_2,new_distance)
            # print('tesuto')
            # pass
        if self.linkage is 'complete':
            # Calculate New Distance
            new_distance = [max(self.distance_matrix[index_1][j],self.distance_matrix[index_2][j]) for j in range(col_size)]
            new_distance[index_1] = 0
            new_distance[index_2] = 0
            self.update2RowCol(index_1,index_2,new_distance)
            # pass
        if self.linkage is 'average':
            n_index1 = len(self.clusters[index_1])
            n_index2 = len(self.clusters[index_2])
            n_new = n_index1+n_index2
            #Calculate New Distance
            new_distance = [(n_index1*self.distance_matrix[index_1][j]+n_index2*self.distance_matrix[index_2][j])/n_new for j in range(col_size)]
            new_distance[index_1] = 0
            new_distance[index_2] = 0
            #Update Distance
            self.update2RowCol(index_1,index_2,new_distance)

        if self.linkage is 'centroid':
            new_cluster_elmt = self.clusters[index_1]+self.clusters[index_2]
            new_cluster_centroid = self.centroid(new_cluster_elmt)
            #Calculate New Distance
            new_distance = [self.distance(new_cluster_centroid,self.centroid(self.clusters[i]),col_size) for i in range(col_size)]
            new_distance[index_1] = 0
            new_distance[index_2] = 0
            #Update Distance
            self.update2RowCol(index_1,index_2,new_distance)
        
        index_removed = max(index_1,index_2)
        index_new_cluster = min(index_1,index_2)
        self.distance_matrix = np.delete(np.delete(self.distance_matrix,index_removed,0),index_removed,1)
    
    def updateCluster(self,index_1,index_2):

        index_removed = max(index_1,index_2)
        index_new_cluster = min(index_1,index_2)
        new_cluster = self.clusters[index_new_cluster]+self.clusters[index_removed]
        
        #Update Hierarchical 
        id_cluster_1 = self.history_clusters.index(self.clusters[index_new_cluster])
        id_cluster_2 = self.history_clusters.index(self.clusters[index_removed])
        self.hierarchical_clustering.append([min(id_cluster_1,id_cluster_2),max(id_cluster_1,id_cluster_2),self.distance_matrix[index_1][index_2],len(new_cluster)])
        #Update Historical
        self.history_clusters.append(copy(new_cluster))

        #Update Distance
        self.updateDistance(index_1,index_2)

        #Update Active Cluster
        self.clusters[index_new_cluster]=new_cluster
        self.clusters.pop(index_removed)
        
        
    def searchMinValueIndex(self):
        
        minValue = np.inf
        min_i, min_j = -1,-1
        row = self.distance_matrix.shape[0]
        for i in range(row):
            for j in range(i):
                if self.distance_matrix[i][j] < minValue:
                    minValue = self.distance_matrix[i][j]
                    min_i, min_j = i,j

        return min_i,min_j

    def agglomerative(self):
        
        min_i,min_j = self.searchMinValueIndex()
        # print("----------------------------------------------")
        # print(self.distance_matrix)
        # print(self.clusters)
        # print(min_i,min_j,self.clusters[min_i],self.clusters[min_j],self.distance_matrix[min_i][min_j])
        
        #If there's only 1 cluster left
        if len(self.clusters) ==1 :
            return
        #Based on Distance Threshold
        if self.distance_threshold is not None :
            #If min_distance is smaller than distance_threshold then return
            if self.distance_threshold < self.distance_matrix[min_i][min_j]:
                return
        #Based on The number of cluster
        else:
            if len(self.clusters) <= self.n_cluster:
                return 
        self.updateCluster(min_i,min_j)

        
        self.agglomerative()

    def fit(self,data):
        # print('masuk sini')
        #Validation
        if self.n_cluster != None and self.n_cluster <= 0:
            raise ValueError("N_cluster harus diatas 0")
        if not ((self.n_cluster is None) ^ (self.distance_threshold is None)):
            raise ValueError("Hanya boleh 1 parameter (n_cluster/distance_threshold) yang di-set nilainya, parameter lainnya harus di-set None")
        if self.linkage not in self.methods:
            raise ValueError("Tipe linkage tidak diimplementasi")
        self.data = data
        self.distance_matrix = self.proximityMatrix(data)
        self.clusters = [[i]for i in range(data.shape[0])]
        self.history_clusters = [[i] for i in range(data.shape[0])]
        self.hierarchical_clustering = []
        self.agglomerative()
        
        self.label = np.array([-1]*self.data.shape[0],dtype=np.int32)
        for cluster_index in range(len(self.clusters)):
            for index in self.clusters[cluster_index]:
                self.label[index] = cluster_index
        self.hierarchical_clustering = np.array(self.hierarchical_clustering)

        return self